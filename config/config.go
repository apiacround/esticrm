package config

import (
	"github.com/pelletier/go-toml"
	"io/ioutil"
	"os"
	"time"
)

type Config struct {
	ApifonicaAPI    *ApifonicaAPI      `toml:"apifonica_api"`
	ServiceSettings *Text2CallSettings `toml:"settings"`
	MongoTTC        MongoSettings      `toml:"mongo_ttc"`
}

type Text2CallSettings struct {
	Port    string `toml:"port"`
	BaseUrl string `toml:"base_url"`
}

type ApifonicaAPI struct {
	Ttl time.Duration `toml:"ttl"`
}

type MongoSettings struct {
	Host     string `toml:"host"`
	Database string `toml:"database"`
	User     string `toml:"user"`
	Password string `toml:"password"`
}

func GetConfigFromFile(filePath string) (*Config, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	cfg := &Config{}
	if err = toml.Unmarshal(b, cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}
