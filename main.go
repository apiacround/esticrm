package main

import (
	"flag"
	"fmt"
	"gitlab.com/aarbenev/estisrc/config"
)

func main() {
	configPath := flag.String("-c", "config-local.toml", "Path to `toml` configuration file")
	flag.Parse()

	c, err := config.GetConfigFromFile(*configPath)
	if err != nil {
		return
	}

}
